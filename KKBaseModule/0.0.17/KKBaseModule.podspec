#
# Be sure to run `pod lib lint KKBaseModule.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KKBaseModule'
  s.version          = '0.0.17'
  s.summary          = 'A short description of KKBaseModule.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/KKBaseModule'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yangpeng' => 'peng.yang@kikitrade.com' }
  s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/KKBaseModule.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'

#  s.source_files = 'KKBaseModule/Classes/**/*'
  s.resources    = 'KKBaseModule/Assets/*'
  
  s.default_subspec = 'BaseVC', 'Helper'
  
  s.subspec 'BaseVC' do |spec|
      spec.source_files = 'KKBaseModule/Classes/BaseVC/**/*.{h,m}'
  end
  
  s.subspec 'Helper' do |spec|
      spec.source_files = 'KKBaseModule/Classes/Helper/**/*.{h,m}'
  end
  
  s.dependency "platform_kiki_container"
   
#  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
#  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  
  # s.resource_bundles = {
  #   'KKBaseModule' => ['KKBaseModule/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
end
