#
# Be sure to run `pod lib lint DuomBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DuomBase'
  s.version          = '0.0.1'
  s.summary          = 'Duom 基础组件&基础架构'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/HeKai_Duom/duombase'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'HeKai_Duom' => 'kai.he@duom.com' }
  s.source           = { :git => 'https://gitlab.com/HeKai_Duom/duombase.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
  s.swift_version    = '5.0'

#  s.source_files = 'DuomBase/Classes/**/*'
  
  s.subspec 'BaseArchitecture' do |spec|
      spec.source_files = 'DuomBase/Classes/BaseArchitecture/**/*'
  end
  
  s.subspec 'BaseComponents' do |spec|
      spec.source_files = 'DuomBase/Classes/BaseComponents/**/*'
  end
  
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'RxRelay'
  s.dependency 'RxOptional'
  s.dependency 'NSObject+Rx'
  s.dependency 'RxViewController'
  s.dependency 'Then'
  s.dependency 'MBProgressHUD'
  s.dependency 'MJRefresh'
  
  
end
